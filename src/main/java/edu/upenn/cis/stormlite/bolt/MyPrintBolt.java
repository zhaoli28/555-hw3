package edu.upenn.cis.stormlite.bolt;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.UUID;

public class MyPrintBolt implements IRichBolt {
    static Logger log = LogManager.getLogger(MyPrintBolt.class);

    Fields myFields = new Fields();

    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the PrintBolt, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();

    @Override
    public void cleanup() {

    }

    @Override
    public boolean execute(Tuple input) {
        if (!input.isEndOfStream())
            System.out.println(getExecutorId() + ": " + input.toString());

        return true;
    }

    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {

    }

    @Override
    public void setRouter(StreamRouter router) {

    }

    @Override
    public Fields getSchema() {
        return myFields;
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
}
