package edu.upenn.cis.stormlite.spout;

import java.io.File;
import java.nio.file.Files;

public class MyFileSpout extends FileSpout {


    @Override
    public String getFilename() {
        if (this.config == null) {  //just for constructor
            return null;
        } else {
            if (filename != null)
                return filename;
            String res = null;
            String dir = config.get("inputDir");
            //look up file in dir
            File folder = new File(dir);
            File[] listOfFiles = folder.listFiles();
            int min = Integer.MAX_VALUE;
            for (File file : listOfFiles) {
                if (file.isFile() && file.getName().length() < min) {
                    min = file.getName().length();
                    res = file.getName();
                }
            }
            System.err.println("--------------"+res);
            return res;
        }
    }
}
