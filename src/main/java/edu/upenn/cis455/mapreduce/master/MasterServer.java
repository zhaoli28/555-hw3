package edu.upenn.cis455.mapreduce.master;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.MyPrintBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.MyFileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static spark.Spark.*;

public class MasterServer {

    static Logger log = LogManager.getLogger(MasterServer.class);

  static final long serialVersionUID = 455555001;
  static final int myPort = 8000;

  static Map<String, List<String>> infoMap = new HashMap<>();

    private static final String WORD_SPOUT = "WORD_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";
  
  public static void registerStatusPage() {
		get("/status", (request, response) -> {
            response.type("text/html");
            String head = "<html><head><title>Master</title></head>\n" +
                          "<body>Hi, I am the master!";

            StringBuilder sb = new StringBuilder();
            sb.append("<table border=\"1\">");
            sb.append("<tr><th>port</th> <th>status</th> <th>job</th> <th>keysRead</th> <th>keysWritten</th> </tr>");
            for (String workerIp: infoMap.keySet()) {
                sb.append("<tr>");
                for (int i = 0; i < infoMap.get(workerIp).size() - 1; i++){ //dont append result
                    sb.append("<td>"+infoMap.get(workerIp).get(i)+"</td>");
                }
                sb.append("</tr>");
            }
            sb.append("</table>");

            String submitForm = "<form action=\"/submitJob\">\n" +
                                "Class name:<br>\n" +
                                "<input type=\"text\" name=\"className\" >\n" +
                                "<br>\n" +
                                "Input Directory:<br>\n" +
                                "<input type=\"text\" name=\"inputDir\" >\n" +
                                "<br>\n" +
                                "Output Directory:<br>\n" +
                                "<input type=\"text\" name=\"outputDir\" >\n" +
                                "<br>\n" +
                                "Number of Map Threads:<br>\n" +
                                "<input type=\"text\" name=\"mapThreads\" >\n" +
                                "<br>\n" +
                                "Number of Reduce Threads:<br>\n" +
                                "<input type=\"text\" name=\"reduceThreads\" >\n" +
                                "<br><br>\n" +
                                "<input type=\"submit\" value=\"Submit\">\n" +
                                "</form> " +
                                "</body></html>";

            return head+sb.toString()+submitForm;
		});
		
  }


    private static void registerSubmitJob() {
        get("/submitJob", (req, res) -> {
            //start doing a job
            String className = req.queryParams("className");
            String inputDir = req.queryParams("inputDir");
            String outputDir = req.queryParams("outputDir");
            String mapThreads = req.queryParams("mapThreads");
            String reduceThreads = req.queryParams("reduceThreads");

            System.err.println(className);
            System.err.println(inputDir);
            System.err.println(outputDir);
            System.err.println(mapThreads);
            System.err.println(reduceThreads);

            //config above things
            Config config = new Config();
            // Complete list of workers, comma-delimited
            config.put("workerList", "[127.0.0.1:8001,127.0.0.1:8002]");
            // Job name
            config.put("job", "MyJob1");
            // IP:port for /workerstatus to be sent
            config.put("master", "127.0.0.1:8080");
            // Class with map function
            config.put("mapClass", className);  //TODO: className
            // Class with reduce function
            config.put("reduceClass", className);//TODO: className
            // Numbers of executors (per node)
            config.put("inputDir", inputDir);
            config.put("outputDir", outputDir);
            config.put("spoutExecutors", "1");
            config.put("mapExecutors", mapThreads);
            config.put("reduceExecutors", reduceThreads);

            FileSpout spout = new MyFileSpout();
            MapBolt bolt = new MapBolt();
            ReduceBolt bolt2 = new ReduceBolt();
            MyPrintBolt printer = new MyPrintBolt();

            TopologyBuilder builder = new TopologyBuilder();

            // Only one source ("spout") for the words
            builder.setSpout(WORD_SPOUT, spout, Integer.valueOf(config.get("spoutExecutors")));
            // Parallel mappers, each of which gets specific words
            builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(config.get("mapExecutors"))).fieldsGrouping(WORD_SPOUT, new Fields("value"));
            // Parallel reducers, each of which gets specific words
            builder.setBolt(REDUCE_BOLT, bolt2, Integer.valueOf(config.get("reduceExecutors"))).fieldsGrouping(MAP_BOLT, new Fields("key"));
            // Only use the first printer bolt for reducing to a single point
            builder.setBolt(PRINT_BOLT, printer, 1).firstGrouping(REDUCE_BOLT);

            Topology topo = builder.createTopology();

            WorkerJob job = new WorkerJob(topo, config);

            ObjectMapper mapper = new ObjectMapper();
            mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
            try {
                String[] workers = WorkerHelper.getWorkers(config);
                int i = 0;
                for (String dest: workers) {
                    System.err.println(dest);
                    config.put("workerIndex", String.valueOf(i++));
                    if (sendJob(dest, "POST", config, "definejob",
                            mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() !=
                            HttpURLConnection.HTTP_OK) {
                        throw new RuntimeException("Job definition request failed");
                    }
                }
                for (String dest: workers) {
                    if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() !=
                            HttpURLConnection.HTTP_OK) {
                        throw new RuntimeException("Job execution request failed");
                    }
                }
            } catch (JsonProcessingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.exit(0);
            }

            res.redirect("/status");
            return null;
        });
    }

    static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {
        URL url = new URL(dest + "/" + job);

        log.info("Sending request to " + url.toString());

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod(reqType);

        if (reqType.equals("POST")) {
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            byte[] toSend = parameters.getBytes();
            os.write(toSend);
            os.flush();
        } else
            conn.getOutputStream();

        return conn;
    }

  /**
   * The mainline for launching a MapReduce Master.  This should
   * handle at least the status and workerstatus routes, and optionally
   * initialize a worker as well.
   * 
   * @param args
   */
    public static void main(String[] args) {
		port(myPort);
		
		System.out.println("Master node startup");

        registerStatusPage();
		// TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce
		// here
		registerSubmitJob();

		
		// TODO: route handler for /workerstatus reports from the workers
        //listen to heartbeats from workers
        registerWorkerStatus();
	}

    private static void registerWorkerStatus() {
        get("/workerstatus", (req, res) -> {
            String ip = req.ip();
            String port = req.queryParams("port");
            String status = req.queryParams("status");
            String jobName = req.queryParams("job");
            String keysRead = req.queryParams("keysRead");
            String keysWritten = req.queryParams("keysWritten");
            String results = req.queryParams("results");

            String[] arr = {port, status, jobName, keysRead, keysWritten, results};
            infoMap.put(ip+":"+port, new ArrayList<>(Arrays.asList(arr)));

            return "heartbeat received";
        });
    }

}
  
